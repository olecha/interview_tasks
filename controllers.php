<?php

function listAction()
{
    $articles = getAllArticles();
    require 'templates/list.php';
}

function showAction($id)
{
    $article = getArticleById($id);
    require 'templates/show.php';
}