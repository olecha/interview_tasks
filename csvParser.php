<?php

function openDatabaseConnection()
{
    $conn = new PDO("mysql:host=localhost;dbname=blog_db", 'myuser', 'mypassword');

    return $conn;
}

function closeDatabaseConnection(&$conn)
{
    $conn = null;
}

function parseCsv(){

    $conn = openDatabaseConnection();
    $row = 1;
    if (($handle = fopen("products.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $num = count($data);
            $row++;
            for ($c=0; $c < $num; $c++) {
                $rowData[$c] = $data[$c];
            }
            $query = "INSERT INTO products(id,name)
                      VALUES('".$rowData[0]."','".$rowData[1]."')";
            $conn->query($query);
        }
        fclose($handle);
    }

    closeDatabaseConnection($conn);
}