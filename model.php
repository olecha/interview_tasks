<?php

function openDatabaseConnection()
{
    $conn = new PDO("mysql:host=localhost;dbname=blog_db", 'myuser', 'mypassword');

    return $conn;
}

function closeDatabaseConnection(&$conn)
{
    $conn = null;
}

function getAllArticles()
{
    $conn = openDatabaseConnection();

    $result = $conn->query('SELECT * FROM article');

    $articles = array();
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $posts[] = $row;
    }
    closeDatabaseConnection($conn);

    return $articles;
}


function getArticleById($id)
{
    $conn = openDatabaseConnection();

    $query = 'SELECT * FROM article WHERE id=:id';
    $statement = $conn->prepare($query);
    $statement->bindValue(':id', $id, PDO::PARAM_INT);
    $statement->execute();

    $row = $statement->fetch(PDO::FETCH_ASSOC);

    closeDatabaseConnection($conn);

    return $row;
}