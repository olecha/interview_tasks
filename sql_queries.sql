CREATE TABLE category (
  id int NOT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE article (
  id   INT          NOT NULL,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE article_category (
  article_id INT NOT NULL,
  category_id INT NOT NULL,
  CONSTRAINT article_cat_pk PRIMARY KEY (article_id, category_id),
  CONSTRAINT FK_article FOREIGN KEY (article_id) REFERENCES article (article_id),
  CONSTRAINT FK_category FOREIGN KEY (category_id) REFERENCES category (category_id)
);
INSERT INTO article
    VALUES (1,'Artykuł1');

INSERT INTO article_category
    VALUES (1,1);

SELECT * FROM article_category
      LEFT JOIN article
      GROUP BY category_id;

SELECT * FROM article_category
  LEFT JOIN article ON article_category.article_id = article.id
ORDER BY category_id;