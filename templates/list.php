<?php $title = 'List of Articles' ?>

<?php ob_start() ?>
    <h1>List of Articles</h1>
    <ul>
        <?php foreach ($articles as $article): ?>
            <li>
                <a href="show.php?id=<?= $article['id'] ?>">
                    <?= $article['title'] ?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
<?php $content = ob_get_clean() ?>

<?php include 'layout.php' ?>