<?php $title = $article['name'] ?>

<?php ob_start() ?>
<h1><?= $article['name'] ?></h1>

<div class="body">
    <?= $article['content'] ?>
</div>
<?php $content = ob_get_clean() ?>

<?php include 'layout.php' ?>